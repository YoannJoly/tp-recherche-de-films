
const wrap = document.getElementById('result')
const main = document.getElementById('main')
const input = document.querySelector('input')
const button = document.querySelector('button')

button.addEventListener('click', onClick);
input.addEventListener("keypress", (event) => {
    if (event.key === 'Enter') {
        onClick()
    }
});

function onClick(j) {
    const myMovie = input.value
    let responseApi = 0

    async function action() {
        wrap.innerHTML = ""
        main.lastChild.innerHTML = ""
        const response = await fetch(`https://www.omdbapi.com/?apikey=180da155&s=${myMovie}&page=${j}`);
        const movies = await response.json();
        return movies
    }

    action()
        .then(movies => responseApi = movies)
        .then(function () {
            let numbers = responseApi.Search.length
            let i = 0
            while (i < numbers) {
                let val = responseApi.Search[i]
                i++
                const block = document.createElement('div')
                block.setAttribute('class', 'container')
                const spanTitle = document.createElement('a')
                spanTitle.href = "#"
                block.appendChild(spanTitle)
                const spanYear = document.createElement('span')
                spanYear.setAttribute('class', 'year')
                const movieImage = document.createElement('img')
                spanTitle.setAttribute('class', 'movieTitle')
                spanTitle.setAttribute('onClick', 'getTitle(this.id)')
                block.appendChild(spanYear)
                block.appendChild(movieImage)
                wrap.appendChild(block)

                spanTitle.textContent = val.Title
                spanTitle.setAttribute('id', `${val.imdbID}`)
                spanYear.textContent = val.Year
                movieImage.src = val.Poster
            }
        }
        )
        .then(function () {
            const nbResult = responseApi.totalResults / 10
            const nav = document.createElement('div')
            nav.setAttribute('class', 'navButtons')
            let v = 0
            while (v < nbResult) {
                v++

                const navButton = document.createElement('a')
                navButton.href = "#"
                navButton.setAttribute('onClick', 'pagination(this.id)')
                navButton.setAttribute('class', 'navButton')
                navButton.textContent = v
                navButton.setAttribute('id', `${v}`)
                navButton.setAttribute('class', `${input.value}`)

                nav.append(navButton)
                main.appendChild(nav)
            }
        }

        )
        .catch(error => console.log("Erreur : " + error));

    input.focus()
}

function pagination(clicked_id) {
    const pages = clicked_id
    console.log(pages)
    onClick(pages)
}

function getTitle(clicked_id) {

    const titles = clicked_id
    main.lastChild.innerHTML = ""
    let resApi = 0

    async function getTitle() {
        wrap.innerHTML = ""
        const response = await fetch(`https://www.omdbapi.com/?i=${titles}&apikey=180da155&plot=full`);
        const newPage = await response.json();
        return newPage;
    }

    getTitle()
        .then(newPage => resApi = newPage)
        .then(function () {

            const backButton = document.createElement('a')
            backButton.setAttribute('class', 'backButton')
            backButton.setAttribute('onClick', 'onClick()')
            backButton.href = '#'
            backButton.textContent = 'Retour'

            const newBlock = document.createElement('div')
            newBlock.setAttribute('class', 'newContainer')

            const poster = document.createElement('img')
            poster.setAttribute('class', 'poster')
            newBlock.appendChild(poster)

            const rightBlock = document.createElement('div')
            rightBlock.setAttribute('class', 'rightBlock')
            newBlock.appendChild(rightBlock)

            const newTitle = document.createElement('h1')
            newTitle.setAttribute('class', 'newTitle')
            rightBlock.appendChild(newTitle)

            const desc = document.createElement('p')
            desc.setAttribute('class', 'description')
            rightBlock.appendChild(desc)

            const note = document.createElement('span')
            note.setAttribute('class', 'note')
            rightBlock.appendChild(note)

            const descYear = document.createElement('span')
            descYear.setAttribute('class', 'descYear')
            rightBlock.appendChild(descYear)

            const duration = document.createElement('span')
            duration.setAttribute('class', 'duration')
            rightBlock.appendChild(duration)

            const casting = document.createElement('span')
            casting.setAttribute('class', 'casting')
            rightBlock.appendChild(casting)

            rightBlock.appendChild(backButton)

            wrap.appendChild(newBlock)

            poster.src = resApi.Poster
            newTitle.textContent = resApi.Title
            desc.textContent = resApi.Plot
            note.textContent = `Note: ${resApi.Metascore}/100`
            descYear.textContent = `Date de sortie: ${resApi.Released}`
            duration.textContent = `Durée: ${resApi.Runtime}`
            casting.textContent = `Casting: ${resApi.Actors}`

        }
        )
        .catch(error => alert("Erreur : " + error));

}